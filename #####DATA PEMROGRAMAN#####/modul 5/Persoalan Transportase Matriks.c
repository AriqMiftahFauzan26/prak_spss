#include<stdio.h>
int main()
{
    int i, j;
    int matrix[3][3], transpose[3][3];

    printf("Masukkan elemen matrix A[3�3]\n");
    for (i=0; i<3; i++)
    {
        for (j=0; j<3; j++)
        {
            printf("M[%d,%d] = ", i+1, j+1);
            scanf("%d", &matrix[i][j]);
        }
    }

    /* TRANSPOSE �> baris jadi kolom dan sebaliknya */
    for (i=0; i<3; i++)
    {
        for (j=0; j<3; j++)
        {
            transpose[j][i] = matrix[i][j];
        }
    }
  
    /* TAMPILKAN TRANSPOSE MATRIK A */
    printf("Transpose Matrik A[3�3]\n");
    for (i=0; i<3; i++)
    {
        for (j=1; j<3; j++)
        {
            printf("%5d", transpose[i][j]);
        } 
        printf("\n");
    }
}
