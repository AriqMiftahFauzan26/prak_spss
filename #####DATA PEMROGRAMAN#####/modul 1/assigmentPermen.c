/*
Program : assigmentPermen.c
Author : 3411201045, Ariq Miftah Fauzan
Deskripsi : program untuk mengisi jumlah permen dan menampilkan jumlah permen ke layar
Tanggal : 23 October 2020
*/

#include <stdio.h>
int main () {
/* Kamus Data */
int nPermen;
/* Algoritma */
nPermen = 10;
printf ("Hai, ada permen sebanyak %d buah\n", nPermen);
return 0;
}
