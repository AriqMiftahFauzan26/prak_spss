/*
Program : bacaChar.c
Author : 3411201045, Ariq Miftah Fauzan
Deskripsi : program membaca karakter kemudian menuliskan nilai yang dibaca kelayar
Tanggal : 23 October 2020
*/
#include <stdio.h>
int main () {
/* Kamus Data */
char huruf;
/* Algoritma */
printf("Contoh membaca dan menulis, ketik nilai character: \n") ;
scanf("%d", huruf);
printf("Nilai yang dibaca %d\n", huruf);
return 1;
}
