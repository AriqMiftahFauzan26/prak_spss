/*
Program : bacaInt.c
Author : 3411201045, Ariq Miftah Fauzan
Deskripsi : program membaca integer kemudian menuliskan nilai yang dibaca kelayar
Tanggal : 23 October 2020
*/

#include <stdio.h>
int main () {
/* Kamus Data */
int bil;
/* Algoritma */
printf("Contoh membaca dan menulis, ketik nilai integer: \n");
scanf("%d", &bil);
printf("Nilai yang dibaca %d\n", bil);
}
