#include <stdio.h>
#include <string.h> 

int main(){
 int n,i;
 char s[1000];
	
 printf ("Masukan banyak kalimat \n");
 scanf("%d",&n);
 if(n<1 || n>10){
  return 0;
 }
 else{
  int bol[n];
	
  printf ("Masukan kalimat \n");
  for(i=0;i<n;i++){
   scanf("%s",s);
   if(cekangka(s)==1){
    return 0;
   }else{
    bol[i]=cekpalin(s);
   }
  }
	
  printf ("\n");
	
  for(i=0;i<n;i++){
   if(bol[i]==1){
    printf ("Palindrom \n");
   }
   else{
    printf ("Bukan Palindrom \n");
   }
  }
 }
}
	
int cekpalin(char s[]){
 int i=0;
 int j=strlen(s)-1;
 if(s[i]==s[j] && s[i+1]==s[j-1]){
  return 1;  
 }else{
  return 0;
 }
}
	
int cekangka(char s[]){
 int j=strlen(s);
 int i=0;
 int b=0;
 while(i<j){
  if (isdigit(s[i])) {
   return 1;
  }
  i++;  
 }  
}
