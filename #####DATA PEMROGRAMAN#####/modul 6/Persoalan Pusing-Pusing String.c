#include <stdio.h>

void operasi(int q,int n, char kal[], int op[], int a[], int b[]); 

int main(){
 int n,q,i;
	
 printf ("Masukan panjang kalimat dan berapa operasi \n");
 scanf("%d %d",&n,&q);
 if(n<2||n>1000){
  return 0;
 }
 else if(q<1||q>1000){
  return 0;
 }
 else{
 char kal[n];
 int op[q];
 int a[q];
 int b[q];
	
 printf ("Masukan kalimat \n");
 scanf("%s",kal);
 printf ("Masukan operasinya 1/2 \n");
 for(i=0;i<q;i++){
  scanf("%d %d %d",&op[i],&a[i],&b[i]);
  if(op[i]<1||op[i]>2){
   return 0;
  }
  else if(a[i]<0||a[i]>n){
   return 0;
  }
  else if(b[i]<0 || b[i]<a[i] ||b[i]>n){
   return 0;
  }
  else{
  operasi(q,n,kal,op,a,b);
  printf ("Hasil operasi %s \n",kal);
  }
 }
 }
}

void operasi(int q,int n, char kal[], int op[], int a[], int b[]){
 int i,temp,s,l;
 for(i=0;i<q;i++){
  s=a[i];
  l=b[i];
  if(op[i]==1){
   temp=kal[a[i]];
   kal[a[i]]=kal[b[i]];
   kal[b[i]]=temp;
  }
  else if(op[i]==2){
   while(s<l){
    temp=kal[s];
    kal[s]=kal[l];
    kal[l]=temp;
    s++;
    l--;
  }
 }
}}
